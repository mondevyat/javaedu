package chapter1.task5;

public class Task5 {
    public static void main(String[] args) {
        double param1 = Double.MAX_VALUE;
        int param2 = (int)param1;
        System.out.println("param1 = " + param1);
        System.out.println("param2 = " + param2);  // произойдёт потеря данных,так как double 8 байт, а int 4
    }
}