package chapter1.task6;

import java.math.BigInteger;

public class Task6 {
    public static void main(String[] args) {
        BigInteger factorial = BigInteger.valueOf(1);
        int n = 1000;
        for (int i = 1; i <= n; i++)
        {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        System.out.println("factorial = " + factorial);
    }
}
